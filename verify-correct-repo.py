#!/usr/bin/python3

# SPDX-FileCopyrightText: 2020 Bhushan Shah <bshah@kde.org>
#
# SPDX-License-Identifier: MIT

"""
This scripts verifies that the job which triggered from correct repository.
"""

import os
import sys
import yaml
import gitlab

print("Testing")

for k, v in os.environ.items():
    print(f'{k}={v}')

if os.environ["CI_PIPELINE_SOURCE"] == 'pipeline':
    print("Pipeline was triggered from another pipeline")

if os.environ["KDE_CI_PROJECT"]:
    print("This job is triggered by the following project:")
    print(os.environ["KDE_CI_PROJECT"])

if os.environ["KDE_CI_PIPELINE_ID"]:
    print("Following pipeline was used:")
    print(os.environ["KDE_CI_PIPELINE_ID"])

# upstream project
project_id = os.environ["KDE_CI_PROJECT"]
# upstream pipeline
pipeline_id = os.environ["KDE_CI_PIPELINE_ID"]

# Now we attempt to get the pipeline status for upstream project
# Connect to gitlab instance
gl = gitlab.Gitlab("https://invent.kde.org/")

# Get project
glProject = gl.projects.get(project_id)

# Get pipeline
glPipeline = glProject.pipelines.get(pipeline_id)

# Now we verify few bits
# This is just example code, in production we want to verify few more bits
# If project is what we generally expect? (in actual production, we would be looking up with our yaml list I suppose)
if glProject.id == 3100:
    print("Repo is exactly what we expect")
else:
    sys.exit(1)

# If pipeline is from "known branch"?
if glPipeline.ref == 'master':
    print("Job is from master branch")
else:
    sys.exit(1)

# Now we download the build
file_name = 'unsigned.txt'
with open(file_name, "wb") as f:
    glProject.artifact('master', 'unsigned.txt', 'build1', streamed=True, action=f.write)

sys.exit(0)
